﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.Services
{
    public class Utilities
    {
        public int ContarDivisores(int numero)
        {
            var contador = 0;
            for (int i = 1; i < numero; i++)
                if (numero % i == 0)
                    contador++;
            return contador;
        }
        

        public class Product
        {
            public int Code { get; set; }
            public string Desc { get; set; }
            public double Value { get; set; }
        }

        public double ObtenerValorProducto(int codigo) 
        { 

        var listProducts = new List<Product>()
            {
                new Product{ Code = 1250, Desc = "MOUSE 3 BOTONES", Value = 15500 },
                new Product{ Code = 1260, Desc = "IMPRESORA LASER", Value = 678000 },
                new Product{ Code = 1270, Desc = "MEMORIA USB 20 GB", Value = 35000 },
                new Product{ Code = 1280, Desc = "DISCO DURO 500 GB", Value = 180000 },
                new Product{ Code = 1290, Desc = "MONITOR 14 PULGADAS", Value = 280000 }

            };

        var Product = listProducts.Where(x => x.Code == codigo).FirstOrDefault();
            if (Product == null)
                return 0;
            return Product.Value;

        }
        public double CalcularSubtotal(double valor, int cantidad)
        {
            var ValorSub = valor * cantidad;
            return ValorSub;
        }
        public double CalcularDescuento(double subtotal)
        {

            double Resultado;

            if (subtotal <= 500000 && subtotal >= 1000000)
            {
                Resultado = (subtotal * 5) / 100;
                return Resultado;
            }
            else if (subtotal > 1000000)
            {
                Resultado = (subtotal * 10) / 100;
                return Resultado;
            }
            {
                return Resultado = 0;
            }


        }
        public double CalcularIVA(double Subtotal)
        {
            double IVA = 0.19;
            double Resultado;

            Resultado = IVA * Subtotal;

            return Resultado;
        }

        public double CalcularTotal(double Subtotal)
        {
            double Total;
            double Impuesto;
            double Descuento;

            if (Subtotal >= 500000 && Subtotal <= 1000000)
            {
                Impuesto = Subtotal * 0.19;
                Descuento = (Subtotal * 5) / 100;
                Total = (Subtotal - Descuento) + Impuesto;

                return Total;

            }
            else if (Subtotal > 1000000)
            {
                Impuesto = Subtotal * 0.19;
                Descuento = (Subtotal * 10) / 100;
                Total = (Subtotal - Descuento) + Impuesto;
                return Total;
            }
            else
            {
                return Total = Subtotal;
            }
        }

        
    }
}
