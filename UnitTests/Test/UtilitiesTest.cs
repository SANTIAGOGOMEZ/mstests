﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests.Test
{
    [TestClass]
    public class UtilitiesTest
    {
        [TestMethod]
        public void ContarDivisoresDeSeis()
        {
            //Arrange
            ApplicationCore.Services.Utilities utilities
                = new ApplicationCore.Services.Utilities();
            var numero = 6;
            //Art
            var result = utilities.ContarDivisores(numero);
            //Assert
            Assert.AreEqual(3, result);
        }

        [TestMethod]
        public void ObtenerValor()
        {
            //Arrange
            ApplicationCore.Services.Utilities utilities
                = new ApplicationCore.Services.Utilities();
            var Product = 1250;
            //Art
            var result = utilities.ObtenerValorProducto(Product);
            //Assert
            Assert.AreEqual(15500, result);
        }
        [TestMethod]
        public void CalcularSubtotalF()
        {
            //Arrange
            ApplicationCore.Services.Utilities utilities
                = new ApplicationCore.Services.Utilities();
            var Value = 280000;
            var Cantidad = 3;
            //Art
            var result = utilities.CalcularSubtotal(Value,Cantidad);
            
            //Assert
            Assert.AreEqual(840000, result);
        }

        [TestMethod]
        public void Descuento()
        {

            //arrange
            ApplicationCore.Services.Utilities utilities 
                = new ApplicationCore.Services.Utilities();
            var subtotal = 840000;

            //Act
            var Resultado_Subtotal = utilities.CalcularDescuento(subtotal);


            //Assert
            Assert.AreEqual(0, Resultado_Subtotal);

        }
        [TestMethod]
        public void IVA()
        {

            //arrange
            ApplicationCore.Services.Utilities utilities = new ApplicationCore.Services.Utilities();
            var Subtotal = 500000;

            //Act
            var IVA = utilities.CalcularIVA(Subtotal);

            //Assert
            Assert.AreEqual(95000, IVA);
        }

        [TestMethod]
        public void Total()
        {

            //arrange
            ApplicationCore.Services.Utilities utilities = new ApplicationCore.Services.Utilities();
            var Subtotal = 400000;

            //Act
            var Total = utilities.CalcularTotal(Subtotal);

            //Assert
            Assert.AreEqual(400000, Total);
        }
    }
}
